FROM maven:3.8.6-openjdk-11-slim

COPY ./target/super-shiny-app-1.0-SNAPSHOT.jar super-shiny-app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","super-shiny-app.jar"]